import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.modelo.Calculadora;


@WebServlet(urlPatterns = {"/CalculadoraController"})
public class CalculadoraController extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CalculadoraController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Bienvenido</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
             System.out.println("pase por el get");
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("pase por el POST");
      
        System.out.println("n1:"+ request.getParameter("n1"));
        System.out.println("n2:"+ request.getParameter("n2")); 
        System.out.println("n3:"+ request.getParameter("n3"));  
        int n1= Integer.parseInt(request.getParameter("n1"));
        int n2= Integer.parseInt(request.getParameter("n2"));
        int n3= Integer.parseInt(request.getParameter("n3"));
       // String  valorDelBoton= request.getParameter("principal");
        Calculadora calculadora=new Calculadora();
        calculadora.setN1(n1);
        calculadora.setN2(n2);
        calculadora.setN3(n3);
        //calculadora.sumar(n1, n2);
         request.setAttribute("calculadora", calculadora);
         request.getRequestDispatcher("resultado.jsp").forward(request, response);
         
         
      /*  if (valorDelBoton.equals("bienvenida")){
            
             request.getRequestDispatcher("bienvenida.html").forward(request, response);
            
        }
        else{
         request.getRequestDispatcher("cumple.html").forward(request, response);   
        }
        */
      
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
